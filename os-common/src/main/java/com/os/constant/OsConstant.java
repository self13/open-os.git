package com.os.constant;

/**
 * 系统 常量
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年1月7日
 */
public interface OsConstant {

	interface DateFormat {
		String YYYYMMDD = "yyyy-MM-dd";
		String YYYYMMDDHH24MMSSS = "yyyyMMddHHmmssSSS";
		String YYYY_MM_DD_HH24_MM_SS = "yyyy-MM-dd HH:mm:ss";
	}

	interface Flag {
		String EQ_SIGN = "=";
		String ONE_AND = "&";
		String TWO_AND = "&&";
		String BRACE = "{}";
		String BLANK = "";
		String COMMA = ",";
		String PERIOD = ".";
		String NO = "NO";
		String U_LINE = "_";
		String WELL = "#";
		String DOLLAR = "$";
		String QUESTION = "?";
		String COLON = ":";
	}

	interface Charset {
		String UTF_8 = "UTF-8";
		String GBK = "GBK";
	}

	interface Path {
		// 登录
		String DEFAULT_LOGIN = "/login";
		// 注销
		String DEFAULT_LOGOUT = "/logout";
		// 图片验证码
		String DEFAULT_IMAGE_CODE = "/code/i";
		// 手机验证码
		String DEFAULT_MOBILE_CODE = "/code/m";
		// 邮箱验证码
		String DEFAULT_EMAIL_CODE = "/code/e";
	}
}
