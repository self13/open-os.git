package com.os.enums;

/**
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
public enum CodeMsgEnum {
	// ------------ 通用（0-50）------------
	SUCCESS(0, "操作成功"), 
	FAILURE(1, "操作失败"), 
	INTERNAL(2, "内部错误");

	CodeMsgEnum(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	private int code;
	private String msg;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
