package com.os.exception;

import java.text.MessageFormat;

import com.os.enums.CodeMsgEnum;

/**
 * 系统异常
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月26日
 */
public class OsException extends RuntimeException {

	private static final long serialVersionUID = 8122436967894232105L;
	private int code;

	public OsException(int code, String message) {
		this(code, message, (Object) null);
	}

	public OsException(int code, String message, Object... details) {
		super(MessageFormat.format(message, details));
		this.code = code;
	}
	
	public OsException(CodeMsgEnum enums){
		this(enums, (Object) null);
	}
	
	public OsException(CodeMsgEnum enums, Object... details) {
		this(enums.getCode(), MessageFormat.format(enums.getMsg(), details));
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
