package com.os.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.os.exception.OsException;

/**
 * ZIP压缩工具
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月26日
 */
public final class ZipUtils {

	public static final String EXT = ".zip";
	private static final String BASE_DIR = "";

	/**
	 * 符号"/"用来作为目录标识判断符
	 */
	private static final String PATH = "/";
	private static final int BUFFER = 1024;

	static Logger logger = LoggerFactory.getLogger(ZipUtils.class);

	private ZipUtils() {
		super();
	}

	/**
	 * @param srcPath
	 */
	public static void compress(String srcPath) {
		File srcFile = new File(srcPath);
		compress(srcFile);
	}
	/**
	 * @param srcFile
	 */
	public static void compress(File srcFile) {
		String name = srcFile.getName();
		String basePath = srcFile.getParent();
		String destPath = basePath + name + EXT;
		compress(srcFile, destPath);
	}

	/**
	 * @param srcFile
	 * @param destPath
	 */
	public static void compress(File srcFile, String destPath) {
		compress(srcFile, new File(destPath));
	}
	
	/**
	 * 文件压缩
	 * @param srcPath 源文件路径
	 * @param destPath 目标文件路径
	 */
	public static void compress(String srcPath, String destPath) {
		File srcFile = new File(srcPath);
		compress(srcFile, destPath);
	}
	
	/**
	 * @param srcFile 源路径
	 * @param destFile 目标路径 
	 */
	public static void compress(File srcFile, File destFile) {
		// 对输出文件做CRC32校验
		ZipOutputStream zos = null;
		try {
			CheckedOutputStream cos = new CheckedOutputStream(new FileOutputStream(destFile), new CRC32());
			zos = new ZipOutputStream(cos);
			compress(srcFile, zos, BASE_DIR);
		} catch (Exception e) {
			logger.error("zip压缩出错", e);
			throw new OsException(-1, e.getMessage());
		} finally {
			try {
				if(null!=zos){
					zos.flush();	
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if(null!=zos){
					zos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param srcFile 源路径
	 * @param zos ZipOutputStream
	 * @param basePath 压缩包内相对路径 @
	 */
	private static void compress(File srcFile, ZipOutputStream zos, String basePath) throws Exception {
		if (srcFile.isDirectory()) {
			compressDir(srcFile, zos, basePath);
		} else {
			compressFile(srcFile, zos, basePath);
		}
	}

	private static void compressDir(File dir, ZipOutputStream zos, String basePath) throws Exception {
		File[] files = dir.listFiles();
		// 构建空目录
		if (files.length < 1) {
			ZipEntry entry = new ZipEntry(basePath + dir.getName() + PATH);
			zos.putNextEntry(entry);
			zos.closeEntry();
		}
		for (File file : files) {
			// 递归压缩
			compress(file, zos, basePath + dir.getName() + PATH);
		}
	}

	private static void compressFile(File file, ZipOutputStream zos, String dir) throws Exception {
		/**
		 * 压缩包内文件名定义
		 *
		 * <pre>
		 * 如果有多级目录，那么这里就需要给出包含目录的文件名
		 * 如果用WinRAR打开压缩包，中文名将显示为乱码
		 * </pre>
		 */
		BufferedInputStream bis = null;
		try {
			ZipEntry entry = new ZipEntry(dir + file.getName());
			zos.putNextEntry(entry);
			bis = new BufferedInputStream(new FileInputStream(file));
			int count;
			byte data[] = new byte[BUFFER];
			while ((count = bis.read(data, 0, BUFFER)) != -1) {
				zos.write(data, 0, count);
			}
		} catch (Exception e) {
			throw e;
		} finally{
			if(null!=bis){
				bis.close();
			}
			if(null!=zos){
				zos.closeEntry();
			}
		}
	}
}