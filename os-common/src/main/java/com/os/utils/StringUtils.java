package com.os.utils;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * String 工具类
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月26日
 */
public final class StringUtils extends org.apache.commons.lang3.StringUtils {
	/** 手机正则 */
	static final String MOBILE_REG = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0-3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$";
	/** 邮箱正则 */
	static final String EMAIL_REG = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

	static Logger logger = LoggerFactory.getLogger(StringUtils.class);
	
	private StringUtils() {
		super();
	}

	public static Boolean isMobile(String mobile) {
		if (isBlank(mobile)) {
			return false;
		}
		return Pattern.matches(MOBILE_REG, mobile);
	}

	public static boolean isEmail(String email) {
		if (isBlank(email)) {
			return false;
		}
		return Pattern.matches(EMAIL_REG, email);
	}

	public static void main(String[] args) {
		System.out.println(isMobile("17008439591"));
		System.out.println(isEmail("550552699@qq.com"));
	}
}
