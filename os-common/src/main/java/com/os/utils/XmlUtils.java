package com.os.utils;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.os.exception.OsException;

/**
 * Xml工具包
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年1月19日
 */
public final class XmlUtils {

	static Logger logger = LoggerFactory.getLogger(XmlUtils.class);

	private XmlUtils() {
		super();
	}

	/**
	 * xml转对象
	 */
	@SuppressWarnings("unchecked")
	public static <T> T xmlToObj(String xml, Class<T> clazz) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xml);

			SAXParserFactory sax = SAXParserFactory.newInstance();
			sax.setNamespaceAware(false);
			XMLReader xmlReader = sax.newSAXParser().getXMLReader();

			Source source = new SAXSource(xmlReader, new InputSource(reader));
			return (T) unmarshaller.unmarshal(source);
		} catch (Exception e) {
			logger.error("xml转对象出错", e);
			throw new OsException(-1, e.getMessage());
		}
	}

	/**
	 * 对象转xml
	 */
	public static String objToXml(Object obj) {
		try {
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			StringWriter writer = new StringWriter();
			marshaller.marshal(obj, writer);
			return writer.toString();
		} catch (Exception e) {
			logger.error("对象转xml出错", e);
			throw new OsException(-1, e.getMessage());
		}
	}
}
