package com.os.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.os.exception.OsException;

/**
 * Date 工具类
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月26日
 */
public final class DateUtils extends org.apache.commons.lang3.time.DateUtils {

	static Logger logger = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
		super();
	}

	public static Date str2Date(String s, String format) {
		SimpleDateFormat simple = new SimpleDateFormat(format);
		try {
			simple.setLenient(false);
			return simple.parse(s);
		} catch (Exception e) {
			logger.error("字符串转日期出错", e);
			throw new OsException(-1, e.getMessage());
		}
	}

	public static String date2Str(Date d, String format) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		try {
			return formater.format(d);
		} catch (Exception e) {
			logger.error("日期转字符串出错", e);
			throw new OsException(-1, e.getMessage());
		}
	}
}
