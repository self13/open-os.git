package com.os.model;

import com.os.enums.CodeMsgEnum;

/**
 * Controller的基础返回类
 *
 * @author XiongNeng
 * @version 1.0
 * @since 2018/1/7
 */
public class Result<T> {

	private int code;
	private String msg;
	private T data;

	public Result() {
		super();
	}

	public Result(int code, String msg, T data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public Result(CodeMsgEnum enums, T data) {
		this(enums.getCode(), enums.getMsg(), data);
	}
	
	public Result(CodeMsgEnum enums) {
		this(enums, null);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
