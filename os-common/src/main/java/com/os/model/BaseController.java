package com.os.model;

import java.text.MessageFormat;

import com.os.enums.CodeMsgEnum;

/**
 * 基础控制层
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年1月3日
 */
public abstract class BaseController {

	protected <T> Result<T> sendMessage(CodeMsgEnum enums, String... values) {
		return this.sendMessage(enums.getCode(), new MessageFormat(enums.getMsg()).format(values));
	}

	protected <T> Result<T> sendMessage(CodeMsgEnum enums) {
		return this.sendMessage(enums.getCode(), enums.getMsg());
	}

	protected <T> Result<T> sendMessage(int code, String msg) {
		return this.send(code, msg, null);
	}

	protected <T> Result<T> sendSuccess() {
		return this.sendSuccess(null);
	}

	protected <T> Result<T> sendSuccess(T model) {
		return this.send(CodeMsgEnum.SUCCESS, model);
	}

	protected <T> Result<T> send(CodeMsgEnum enums, T model) {
		return this.send(enums.getCode(), enums.getMsg(), model);
	}

	protected <T> Result<T> send(int code, String msg, T model) {
		return new Result<T>(code, msg, model);
	}
}
