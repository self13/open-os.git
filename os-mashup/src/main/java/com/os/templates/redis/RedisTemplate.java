package com.os.templates.redis;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

/**
 * 重新定义 redis模板
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
@Component("osRedisTemplate")
public class RedisTemplate<V> implements Serializable {

	private static final long serialVersionUID = 7454427897883956723L;
	/** 默认时间 */
	public final static long DEFAULT_EXPIRE_TIME = 1 * 60 * 60;
	/** 默认时间单位 */
	public final static TimeUnit DEFAULT_EXPIRE_TIMEUNIT = TimeUnit.SECONDS;
	/** 不设置过期时长 */
	public final static long NOT_EXPIRE = 0;
	@Resource(name = "redisTemplate")
	private org.springframework.data.redis.core.RedisTemplate<String, V> redisTemplate;

	/**
	 * 缓存放入
	 */
	public void put(final String k, V v, long expire, TimeUnit timeUnit) {
		ValueOperations<String, V> vo = redisTemplate.opsForValue();
		if (expire > NOT_EXPIRE) {
			vo.set(k, v, expire, timeUnit);
		} else {
			vo.set(k, v);
		}
	}

	public void put(final String k, V v, long expire) {
		this.put(k, v, expire, DEFAULT_EXPIRE_TIMEUNIT);
	}

	public void put(final String k, V v) {
		this.put(k, v, NOT_EXPIRE);
	}

	/**
	 * 从缓存中获得 对象
	 */
	public V get(final String k) {
		ValueOperations<String, V> vo = redisTemplate.opsForValue();
		return vo.get(k);
	}

	/**
	 * 从缓存中移除 对象
	 */
	public Boolean remove(final String k) {
		return redisTemplate.delete(k);
	}

	/**
	 * 是否缓存了对象
	 */
	public Boolean hasKey(final String k) {
		return redisTemplate.hasKey(k);
	}

	/**
	 * 获得缓存存活时长
	 */
	public Long getExpire(final String k, TimeUnit timeUnit) {
		return redisTemplate.getExpire(k, timeUnit);
	}

	public Long getExpire(final String k) {
		return redisTemplate.getExpire(k, DEFAULT_EXPIRE_TIMEUNIT);
	}

	/**
	 * 缓存存活时长设置
	 */
	public void expire(final String k, long expire, TimeUnit timeUnit) {
		redisTemplate.expire(k, expire, timeUnit);
	}

	public void expire(final String k, long expire) {
		expire(k, expire, DEFAULT_EXPIRE_TIMEUNIT);
	}

	/**
	 * 加锁
	 */
	public Boolean lock(String k, V v, long time, TimeUnit timeUnit) {
		boolean result = redisTemplate.opsForValue().setIfAbsent(k, v);
		if (result) {
			if (time > NOT_EXPIRE) {
				expire(k, time, timeUnit);
			} else {
				expire(k, DEFAULT_EXPIRE_TIME);
			}
		}
		return result;
	}

	public Boolean lock(String k, V v, long time) {
		return lock(k, v, time, DEFAULT_EXPIRE_TIMEUNIT);
	}

	public Boolean lock(String k, V v) {
		return lock(k, v, DEFAULT_EXPIRE_TIME);
	}

	/**
	 * 释放锁
	 */
	public Boolean unLock(String k) {
		return remove(k);
	}
}
