package com.os.templates.redis;

import java.io.Serializable;
import java.time.Duration;

import javax.annotation.Resource;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
@Configuration("osRedisConfig")
public class RedisConfig<V> implements Serializable {

	private static final long serialVersionUID = -8395705209410902926L;
	@Resource(name = "osRedisProperty")
	private RedisProperty redisProperty;

	/**
	 * redis 原生模板类
	 */
	@Bean(name = "redisTemplate")
	public RedisTemplate<String, V> redisTemplate() {
		RedisTemplate<String, V> redisTemplate = new RedisTemplate<String, V>();
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new JdkSerializationRedisSerializer());
		redisTemplate.setHashValueSerializer(new JdkSerializationRedisSerializer());
		redisTemplate.setConnectionFactory(localLettuceConnectionFactory());
		return redisTemplate;
	}

	/**
	 * redis 连接工厂
	 */
	@Bean
	public LettuceConnectionFactory localLettuceConnectionFactory() {
		LettuceClientConfiguration clientConfig = LettucePoolingClientConfiguration.builder()
				.commandTimeout(Duration.ofMillis(redisProperty.getTimeout())).poolConfig(redisPoolConfig()).build();
		return new LettuceConnectionFactory(redisConfig(), clientConfig);
	}

	/**
	 * redis 连接池
	 */
	@Bean
	public GenericObjectPoolConfig redisPoolConfig() {
		GenericObjectPoolConfig config = new GenericObjectPoolConfig();
		config.setMaxTotal(redisProperty.getMaxActive());
		config.setMaxIdle(redisProperty.getMaxIdle());
		config.setMinIdle(redisProperty.getMinIdle());
		config.setMaxWaitMillis(redisProperty.getMaxWait());
		return config;
	}

	/**
	 * redis DB
	 */
	@Bean
	public RedisStandaloneConfiguration redisConfig() {
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
		config.setHostName(redisProperty.getHost());
		config.setPassword(RedisPassword.of(redisProperty.getPassword()));
		config.setPort(redisProperty.getPort());
		config.setDatabase(redisProperty.getDatabase());
		return config;
	}
}
