package com.os.templates.redis;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * redis
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年1月18日
 */
@Component("osRedisProperty")
public class RedisProperty implements Serializable {

	private static final long serialVersionUID = 5670812682696401209L;
	@Value("${os.redis.host:127.0.0.1}")
	private String host;
	@Value("${os.redis.port:6379}")
	private Integer port;
	@Value("${os.redis.password:}")
	private String password;
	@Value("${os.redis.database:0}")
	private Integer database;
	@Value("${os.redis.timeout:1000}")
	private Integer timeout;
	@Value("${os.redis.lettuce.pool.max-active:8}")
	private Integer maxActive;
	@Value("${os.redis.lettuce.pool.max-idle:8}")
	private Integer maxIdle;
	@Value("${os.redis.lettuce.pool.max-wait:-1}")
	private Long maxWait;
	@Value("${os.redis.lettuce.pool.min-idle:0}")
	private Integer minIdle;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getDatabase() {
		return database;
	}

	public void setDatabase(Integer database) {
		this.database = database;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(Integer maxActive) {
		this.maxActive = maxActive;
	}

	public Integer getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	public Long getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(Long maxWait) {
		this.maxWait = maxWait;
	}

	public Integer getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}
}
