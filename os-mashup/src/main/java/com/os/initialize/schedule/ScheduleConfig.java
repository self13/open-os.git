package com.os.initialize.schedule;

import java.io.Serializable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年7月11日
 */
@Configuration("osScheduleConfig")
public class ScheduleConfig implements SchedulingConfigurer, Serializable {

	private static final long serialVersionUID = 7802421788557143855L;

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(scheduleExecutor());
	}

	@Bean(destroyMethod = "shutdown")
	public ScheduledExecutorService scheduleExecutor() {
		return Executors.newScheduledThreadPool(50);// 项目核心线程数大小50
	}
}
