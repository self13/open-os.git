package com.os.initialize.swagger2;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.google.common.collect.Lists;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date  2019年6月27日
 */
@Configuration("osSwagger2Config")
public class Swagger2Config implements WebMvcConfigurer, Serializable{

	private static final long serialVersionUID = -890476588360428222L;
	@Resource(name = "osSwagger2Property")
	private Swagger2Property p;

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
				.paths(PathSelectors.any())
				.build()
				.securitySchemes(security());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(p.getTitle())
				.description(p.getDescription())
				.termsOfServiceUrl(p.getTermsOfServiceUrl())
				.version(p.getVersion())
				.build();
	}

	private List<ApiKey> security() {
		return Lists.newArrayList(new ApiKey("token", "token", "header"));
	}
	
	/**
	 * swagger2 静态资源解析
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/js/**").addResourceLocations("classpath:/js/");
		registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
}
