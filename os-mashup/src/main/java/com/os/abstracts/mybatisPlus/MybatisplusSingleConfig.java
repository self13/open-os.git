package com.os.abstracts.mybatisPlus;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * DB单数据源公共抽象配置
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
public abstract class MybatisplusSingleConfig extends MybatisplusConfig {

	private static final long serialVersionUID = 7510028963308944832L;
	static final Logger logger = LoggerFactory.getLogger(MybatisplusSingleConfig.class);

	/**
	 * 根据dbProperty 获得数据源
	 */
	protected DataSource dataSource(MybatisplusProperty dbProperty) {
		DruidDataSource ds = new DruidDataSource();
		ds.setUrl(dbProperty.getUrl());
		ds.setUsername(dbProperty.getUsername());
		ds.setPassword(dbProperty.getPassword());
		ds.setDriverClassName(dbProperty.getDriverClassName());
		ds.setInitialSize(dbProperty.getInitialSize());
		ds.setMaxActive(dbProperty.getMaxActive());
		ds.setMinIdle(dbProperty.getMinIdle());
		ds.setMaxWait(dbProperty.getMaxWait());
		ds.setPoolPreparedStatements(dbProperty.getPoolPreparedStatements());
		ds.setMaxPoolPreparedStatementPerConnectionSize(dbProperty.getMaxPoolPreparedStatementPerConnectionSize());
		ds.setValidationQuery(dbProperty.getValidationQuery());
		ds.setValidationQueryTimeout(dbProperty.getValidationQueryTimeout());
		ds.setTestOnBorrow(dbProperty.getTestOnBorrow());
		ds.setTestOnReturn(dbProperty.getTestOnReturn());
		ds.setTestWhileIdle(dbProperty.getTestWhileIdle());
		ds.setTimeBetweenEvictionRunsMillis(dbProperty.getTimeBetweenEvictionRunsMillis());
		ds.setMinEvictableIdleTimeMillis(dbProperty.getMinEvictableIdleTimeMillis());
		ds.setUseGlobalDataSourceStat(true);
		try {
			ds.setFilters(dbProperty.getFilters());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("druid configuration initialization filter", e);
		}
		return ds;
	}
}
