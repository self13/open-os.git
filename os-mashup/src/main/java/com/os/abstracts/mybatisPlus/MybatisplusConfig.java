package com.os.abstracts.mybatisPlus;

import java.io.Serializable;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

/**
 * DB数据源公共抽象配置
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
public abstract class MybatisplusConfig implements Serializable {

	private static final long serialVersionUID = -2521548189095346787L;

	protected abstract DataSource dataSource(MybatisplusProperty dbProperty);

	protected SqlSessionFactory sqlSessionFactory(DataSource dataSource, String mapperLocations) throws Exception {
		GlobalConfig globalconfig = new GlobalConfig();
		GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
		dbConfig.setIdType(IdType.valueOf("AUTO"));// 数据库主键自增
		dbConfig.setDbType(DbType.valueOf("MYSQL"));// 数据库类型MySql
		dbConfig.setTableUnderline(true);// 表下划线处理
		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
		// 1、设置dataSource
		bean.setDataSource(dataSource);
		// 2、设置扫描映射xml文件
		bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));
		// 3、设置全局配置
		bean.setGlobalConfig(globalconfig);
		// 4、设置插件-分页
		bean.setPlugins(new Interceptor[] { new PaginationInterceptor() });
		return bean.getObject();
	}

	protected SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}