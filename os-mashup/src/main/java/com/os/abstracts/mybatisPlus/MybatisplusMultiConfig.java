package com.os.abstracts.mybatisPlus;

import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;

/**
 * 多数据源
 * 
 * @ename wade.nie
 * @cname 聂敏
 * @email 550552699@qq.com
 * @date 2019年6月27日
 */
public abstract class MybatisplusMultiConfig extends MybatisplusConfig {

	private static final long serialVersionUID = 5206634976208801324L;
	static final Logger logger = LoggerFactory.getLogger(MybatisplusMultiConfig.class);

	/**
	 * 根据SourceProperty 获得数据源
	 */
	protected DataSource dataSource(MybatisplusProperty dbProperty) {
		AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
		Properties prop = new Properties();
		prop.put("url", dbProperty.getUrl());
		prop.put("username", dbProperty.getUsername());
		prop.put("password", dbProperty.getPassword());
		prop.put("driverClassName", dbProperty.getDriverClassName());
		prop.put("initialSize", dbProperty.getInitialSize());
		prop.put("maxActive", dbProperty.getMaxActive());
		prop.put("minIdle", dbProperty.getMinIdle());
		prop.put("maxWait", dbProperty.getMaxWait());
		prop.put("poolPreparedStatements", dbProperty.getPoolPreparedStatements());
		prop.put("maxPoolPreparedStatementPerConnectionSize", dbProperty.getMaxPoolPreparedStatementPerConnectionSize());
		prop.put("validationQuery", dbProperty.getValidationQuery());
		prop.put("validationQueryTimeout", dbProperty.getValidationQueryTimeout());
		prop.put("testOnBorrow", dbProperty.getTestOnBorrow());
		prop.put("testOnReturn", dbProperty.getTestOnReturn());
		prop.put("testWhileIdle", dbProperty.getTestWhileIdle());
		prop.put("timeBetweenEvictionRunsMillis", dbProperty.getTimeBetweenEvictionRunsMillis());
		prop.put("minEvictableIdleTimeMillis", dbProperty.getMinEvictableIdleTimeMillis());
		prop.put("filters", dbProperty.getFilters());
		ds.setXaProperties(prop);
		ds.setXaDataSourceClassName(dbProperty.getDataSourceClassName());
		ds.setUniqueResourceName(dbProperty.getUniqueResourceName());
		ds.setMaxPoolSize(dbProperty.getMaxActive());
		return ds;
	}
}